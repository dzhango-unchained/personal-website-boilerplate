import React, { Component } from 'react';
import Hero from 'proto-hero';
import Value from 'proto-values';
import { ZoomModal } from '../../components';
import styles from './styles.css';
import data from '../../site.config.json';

class IndexPage extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	renderValues() {
		return data.values.map((value) => (
			<Value
				key={value.title}
				title={value.title}
				subtitle={value.subtitle}
				description={value.description}
			/>
		));
	}
	renderHelpButtons() {
		return data.helpButtons.map((help, index) => (
				<a 
					key={index}
					className={`button button-primary ${styles.customButton}`} 
					href={help.link}
					style={{ 
						backgroundColor: help.color,
						borderColor: help.color
					}}
				>
					{help.text}
				</a>
		));
	}
	renderCategoriesImages() {
		return data.categoriesImages.map((images, index) => (
			<div className='three columns category' key={index}>
				<p>{images.title}</p>
				<img 
					alt='none' 
					src={images.src} 
					className='u-max-full-width'
					onClick={() => this.setState({ [images.src]: true })}
				/>
				<ZoomModal 
					isShowingModal={this.state[images.src]}
					onClose={() => this.setState({ [images.src]: false })}
					src={images.src}
				/>
			</div>
		));
	}
	render() {
		return (
			<div>
				<Hero 
					heading={data.heading}
					imgSrc={data.heroImg}
					links={data.links}
				/>
				{/* <===== A section for rendering values (Must be 3 values) =====> */}
				<div 
					className={`${styles.section} ${styles.values}`}
					style={{
						backgroundImage: `url(${data.valuesImg})`
					}}
				>
					<div className="container">
						<div className="row">
							{ this.renderValues() }
						</div>
					</div>
				</div>
				{/* <===== A section for rendering other links =====> */}
				<div className={`${styles.section} ${styles.getHelp}`}>
					<h3 className={styles.sectionHeading}>
						{data.helpHeading}
					</h3>
					<hr />
					<p className={styles.sectionDescription}>
						{data.helpDescription}
					</p>
					{ this.renderHelpButtons() }
				</div>
				{/* <===== A section for rendering other images =====> */}
				<div 
					className={`${styles.section} ${styles.categories}`}
					style={{
						backgroundImage: `url(${data.valuesImg})`
					}}
				>
					<div className="container">
						<h3 className={`${styles.categoriesHeading}`}>
							{data.categoriesHeading}
						</h3>
						<p className={`${styles.sectionDescription}`}>
							{data.categoriesDescription}
						</p>
						<br />
						<hr />
						<div className='row'>
							{ this.renderCategoriesImages() }
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default IndexPage;
