import React, { Component } from 'react';
import { ModalDialog, ModalContainer } from 'react-modal-dialog';
import styles from './styles.css';

class ZoomModal extends Component {
	render() {
		return (
			<div>
				{
					this.props.isShowingModal &&
					<ModalContainer onClose={this.props.onClose}>
						<ModalDialog onClose={this.props.onClose}>
							<img className={styles.zoomImage} alt='none' src={this.props.src} />
						</ModalDialog>
					</ModalContainer>
				}
			</div>
		);
	}
}

export default ZoomModal;
